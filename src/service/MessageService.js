const fakeUrl = 'https://edikdolynskyi.github.io/react_sources/messages.json';

export const api = {
    messages: {
        async fetchMessages() {
            const response = await fetch(fakeUrl);
            if (response.status === 200) {
                const data = await response.json();
                return data;
            }
        }
    }
}