import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './commponent/Chat/Chat';
import Chat from "./commponent/Chat/Chat";
import {store} from "./redux/redux-store";
import {Provider} from "react-redux";


ReactDOM.render(
    <Provider store={store}>
        <Chat />
    </Provider>,
    document.getElementById('root')
);
