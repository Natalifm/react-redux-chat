import React, {Component} from 'react';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import CssBaseline from '@material-ui/core/CssBaseline';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Header from "../Header/Header";
import AllUsersMessages from "../UsersMessages/AllUsersMessages";
import TextInput from "../TextInput/TextInput";
import Logo from "../Logo/Logo";
import Footer from "../Footer/Footer";
import Modal from "../Modal/Modal";

import {modalActions} from "../../redux/modalReducer/reducer";
import {messagesActions} from "../../redux/messageReducer/reducer";

import './App.css';

class Chat extends Component {
    componentDidMount() {
        const {actions} = this.props;
        actions.fetchMessages();
    }

    render() {
        const {messages, editingMessageId, isModalOpen, actions} = this.props;
        const participants = (new Set(messages.map(({user}) => user))).size;
        const lastMessageTime = messages.length && messages[messages.length - 1].createdAt;
        return (
            <>
                {messages.length
                    ? <>
                        <Logo/>
                        <Header
                            usersmessages={messages.length}
                            participants={participants}
                            lastMessageTime={lastMessageTime}
                        />
                        <CssBaseline/>
                        <Container maxWidth="md">
                            <Modal
                                messages={messages}
                                editingMessageId={editingMessageId}
                                isModalOpen={isModalOpen}
                                updateMessage={actions.updateMessage}
                                toggleModalOpen={actions.toggleModalOpen}
                            />
                            <AllUsersMessages
                                messages={messages}
                                toggleMessageLike={actions.toggleMessageLike}
                                deleteMessage={actions.deleteMessage}
                                setEditingMessageId={actions.setEditingMessageId}
                                toggleModalOpen={actions.toggleModalOpen}
                            />
                            <TextInput createMessage={actions.createMessage}/>
                            <Footer/>
                        </Container>
                    </>
                    : <CircularProgress
                        className='loading'
                        color='secondary'
                        size={80}
                        thickness={6}
                    />
                }
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages,
        editingMessageId: state.modal.editingMessageId,
        isModalOpen: state.modal.isModalOpen
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators({
            ...messagesActions,
            ...modalActions
        }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
