import React, {Fragment} from 'react';
import Divider from '@material-ui/core/Divider';
import Message from "../Message/Message";
import {array, func} from 'prop-types';

const UsersMessages = ({messages, deleteMessage, toggleMessageLike, setEditingMessageId, toggleModalOpen}) => {
    const createUsersMessages = () => {
        return messages.map((post, index) => {
            const card = <Message
                key={post.id}
                post={post}
                deleteMessage={deleteMessage}
                toggleMessageLike={toggleMessageLike}
                setEditingMessageId={setEditingMessageId}
                toggleModalOpen={toggleModalOpen}/>;

            if (messages[index + 1] && (new Date(messages[index + 1].createdAt)).getDate() - (new Date(messages[index].createdAt)).getDate()) {

                return (
                    <Fragment key={post.id}>
                        {card}
                        <Divider style={{marginTop: 20}}/>
                        <p style={{textAlign: 'center'}}>{(new Date(messages[index + 1].createdAt))
                            .toLocaleString('en', {month: 'long', day: 'numeric'})}</p>
                    </Fragment>
                )
            } else {
                return card;
            }
        })
    }

    const allUsersMessages = createUsersMessages();

    return (
        <>
            {allUsersMessages}
        </>
    )
}

UsersMessages.propTypes = {
    messages: array.isRequired,
    toggleMessageLike: func.isRequired,
    deleteMessage: func.isRequired,
    setEditingMessageId: func.isRequired,
    toggleModalOpen: func.isRequired,
}

export default UsersMessages;
