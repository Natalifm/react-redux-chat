import React from 'react';
import Card from '@material-ui/core/Card';
import Avatar from '@material-ui/core/Avatar';
import {Delete, FavoriteBorder, Settings} from '@material-ui/icons';
import {func, bool, string, shape} from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';

import './Message.css';

const Message = ({post, toggleMessageLike, deleteMessage, setEditingMessageId, toggleModalOpen}) => {
    const {id, avatar, createdAt, text, user, isLiked} = post;

    const favoriteBorderColor = isLiked ? '#f00' : '#000';
    const myMessage = user === 'me' ? 'auto' : '0';

    const handleEditMessage = () => {
        setEditingMessageId(id);
        toggleModalOpen();
    }

    const createDeleteIcon = () => user === 'me'
        ? <Delete
            className='delete'
            onClick={() => deleteMessage(id)}/>
        : null;
    const favoriteBorderIcon = () => user !== 'me'
        ? <FavoriteBorder
            className='like'
            style={{color: favoriteBorderColor}}
            onClick={() => toggleMessageLike(id)}/>
        : null;
    const card = user === 'me'
        ? <IconTooltip
            placement="left"
            interactive={true}
            title={
                <>
                    <Settings
                        onClick={handleEditMessage}
                    />
                </>
            }>
            <Card
                className='card'
                style={{marginLeft: myMessage}}>
                {avatar && <Avatar src={avatar} alt="user" style={{margin: 10}}/>}
                <div style={{width: '80%', maxWidth: '100%'}}>
                    <p>
                        {createdAt}
                    </p>
                    <p>{text}</p>
                </div>
                {createDeleteIcon()}
                {favoriteBorderIcon()}
            </Card>
        </IconTooltip>
        : <Card
            className='card'
            style={{marginLeft: myMessage}}>
            {avatar && <Avatar src={avatar} alt="user" style={{margin: 10}}/>}
            <div style={{width: '80%', maxWidth: '100%'}}>
                <p>
                    {createdAt}
                </p>
                <p>{text}</p>
            </div>
            {createDeleteIcon()}
            {favoriteBorderIcon()}
        </Card>
    return card;
}

const IconTooltip = withStyles(theme => ({
    tooltip: {
        backgroundColor: '#e75677',
        color: 'rgba(80,75,75,0.87)',
        maxWidth: 220,
        border: '1px solid #dadde9'
    },
}))(Tooltip);

Message.propTypes = {
    post: shape({
        id: string.isRequired,
        avatar: string,
        createdAt: string.isRequired,
        text: string.isRequired,
        user: string.isRequired,
        isLiked: bool
    }),
    toggleMessageLike: func.isRequired,
    deleteMessage: func.isRequired,
    setEditingMessageId: func.isRequired,
    toggleModalOpen: func.isRequired
}

export default Message;
