import React, {useState} from 'react';
import uuid from 'react-uuid'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {func} from 'prop-types';
import './TextInput.css';

const TextInput = (props) => {
    const [text, setText] = useState('');

    const handleCreateMessage = (e) => {
        const createdAt = new Date().toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit'})

        e.preventDefault();
        if (!(text.trim())) {
            return;
        }
        props.createMessage({
            id: uuid(),
            user: "me",
            createdAt,
            text,
            isLiked: false
        });
        setText('');
    }

    return (
        <>
            <form
                onSubmit={handleCreateMessage}
                className='form'>
                <TextField
                    className="textInput"
                    label="Text message"
                    variant="filled"
                    color="secondary"
                    value={text}
                    onChange={(e) => setText(e.target.value)}
                />
                <Button className="inputBtn" variant="contained" color="secondary" size="small" type="submit">
                    Send
                </Button>
            </form>
        </>
    )
}

TextInput.propTypes = {
    createMessage: func.isRequired
}

export default TextInput;
