import {applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

const middleware = [thunk];
const enhancedStore = (applyMiddleware(...middleware));

export {enhancedStore};