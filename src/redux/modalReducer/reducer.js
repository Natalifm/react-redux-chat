const TOGGLE_MODAL_OPEN = "TOGGLE-MODAL-OPEN";
const SET_EDITING_MESSAGE_ID = "SET-EDITING-MESSAGE-ID";

const initialState = {
    isModalOpen: false,
    editingMessageId: ''
};

export const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_MODAL_OPEN:
            return {
                ...state,
                isModalOpen: !state.isModalOpen
            }

        case SET_EDITING_MESSAGE_ID:
            return {
                ...state,
                editingMessageId: action.payload
            }

        default:
            return state;
    }
}

export const modalActions = {
    toggleModalOpen: () => ({
        type: TOGGLE_MODAL_OPEN
    }),

    setEditingMessageId: (id) => ({
        type: SET_EDITING_MESSAGE_ID,
        payload: id
    }),
};