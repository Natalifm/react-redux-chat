import {api} from "../../service/MessageService";

const FETCH_MESSAGES = "FETCH-MESSAGES";
const ALL_MESSAGES = "ALL-MESSAGES";
const CREATE_MESSAGE = "CREATE-MESSAGE";
const LIKE_MESSAGE = "LIKE-MESSAGE";
const DELETE_MESSAGE = "DELETE-MESSAGE";
const UPDATE_MESSAGE = "UPDATE-MESSAGE";

const initialState = [];

export const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ALL_MESSAGES:
            return [...action.payload];

        case CREATE_MESSAGE:
            return [...state, action.payload];

        case LIKE_MESSAGE:
            return state.map(message => message.id !== action.payload
                ? message
                : {
                    ...message,
                    isLiked: !message.isLiked
                }
            )

        case DELETE_MESSAGE:
            return state.filter(message => message.id !== action.payload);

        case UPDATE_MESSAGE:
            return state.map(post => post.id !== action.payload.id
                ? post
                : {
                    ...post,
                    text: action.payload.text
                }
            );

        default:
            return state;
    }
}

export const messagesActions = {
    fetchMessages: () => async (dispatch) => {
        try {
            dispatch({
                type: FETCH_MESSAGES
            })
            const data = await api.messages.fetchMessages();
            dispatch(messagesActions.allMessages(data));
        } catch (error) {
            console.log('NETWORK ERROR', error.message);
        }
    },

    allMessages: (messages) => ({
        type: ALL_MESSAGES,
        payload: messages
    }),

    createMessage: (message) => ({
        type: CREATE_MESSAGE,
        payload: message
    }),

    toggleMessageLike: (id) => ({
        type: LIKE_MESSAGE,
        payload: id
    }),

    deleteMessage: (id) => ({
        type: DELETE_MESSAGE,
        payload: id
    }),

    updateMessage: (id, text) => ({
        type: UPDATE_MESSAGE,
        payload: {id, text}
    }),
};