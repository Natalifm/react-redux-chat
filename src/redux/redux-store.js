import {combineReducers, createStore} from 'redux';
import {enhancedStore} from './middleware';
import {messagesReducer as messages} from './messageReducer/reducer';
import {modalReducer as modal} from './modalReducer/reducer';

const redusers = combineReducers({
    messages,
    modal
})

export const store = createStore(redusers, enhancedStore);
